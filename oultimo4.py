vendedor = {
	"salario" : float(input("Digite o salário fixo do funcionário: ")),
	"vendas" : [],
	"totalVendido" : 0.0,
	"salarioTotal" : 0.0
}

n = int(input("Quantas vendas foram feitas pelo funcionário? "))

for i in range(1, n+1) :
	venda = float(input("Venda %i, valor: " %(i))) 
	vendedor["vendas"].append(venda)
	
for v in vendedor["vendas"] :
	vendedor["totalVendido"] += v
	
if (vendedor["totalVendido"] > 1500.00) :
	vendedor["salarioTotal"] += vendedor["salario"] + (1500.00 * 0.03) + (vendedor["totalVendido"] - 1500.00) * 0.05
else :
	vendedor["salarioTotal"] += vendedor["salario"] + (vendedor["totalVendido"] * 0.03)

print("Salario total do funcionário: R$ %.2f " %(vendedor["salarioTotal"]))
	


